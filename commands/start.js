const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "start",
  description: "Everything starts here, welcome!",
  //usage: "",
  //aliases: ["", ""],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot) => {
    let playerFound = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription('You have already started your adventure! Why bother coming back to your starting point?');
    if (db.fetch(message.author.id) !== null) return message.channel.send(playerFound);

    let newPlayer = {
      name: "",
      busy: false,
      hp: 100,
      maxHp: 100,
      addedDamage: 0,
      xp: 0,
      level: 1,
      nextLevel: 100,
      inventory: [],
      gold: 0,
      diamonds: 0,
      crystalShards: 0,
      mana: 100,
      maxMana: 100,
      magicAttacks: [],
      currentMagicAttack: null,
      stamina: 20,
      maxStamina: 20,
      weapons: [],
      currentWeapon: null
    }

    const filter = m => m.author.id === message.author.id;

    let nameCollect = new Discord.MessageEmbed()
      .setColor(bot.config.color.blue)
      .setDescription(`${message.author}, What shall be your name in this world? (4-32 characters)`)
      .setFooter('Reply with "cancel" to cancel this.');

    message.channel.send(nameCollect);

    message.channel.awaitMessages(filter, { max: 1, time: 15000 }).then(collected => {

      let col1;
      try {
        col1 = collected.first().content;

        if (col1.length < 4 || col1.length > 32) throw new Error('Name must be between 4 and 32 characters!');
      } catch {
        let nameErr = new Discord.MessageEmbed()
          .setColor(bot.config.color.red);

        let nameArr = [
          'Serf Ouin',
          'Count Roley',
          'Margrave Milet',
          'Earl Symonet',
          'Bishop Houdin',
          'Baron Estevenot',
          'Admiral Remfrey',
          'Count Simcock',
          'Baronet Curtis',
          'King Theodric',
          'Serf Belsante',
          'Queen Liza',
          'Countess Mehenilda',
          'Dame Grisel',
          'Princess Consort Atheena',
          'Viscountess Oriold',
          'Maiden Haoys',
          'Baroness Eloise',
          'Baronetess Grissel',
          'Baroness Gabriel'
        ]

        col1 = bot.f.arrayRandom(nameArr);

        nameErr.setDescription(`Looks like I\'ll have to give you a name since one of these options was met:\n- Name doesn't fit character limit\n- You didn't tell me a name\n\n**${col1}** sounds like a good one :)`);

        message.channel.send(nameErr);
      }
      if (col1 === "cancel") {
        let cancelEmbed = new Discord.MessageEmbed()
          .setColor(bot.config.color.green)
          .setDescription(`Alright... :/`);

        return message.channel.send(cancelEmbed);
      }

      newPlayer.name = col1;
      db.set(message.author.id, newPlayer);

      bot.f.giveWeapon('Knife', message.author.id);
      bot.f.giveWeapon('Green Orb', message.author.id);

      let player = db.fetch(message.author.id);

      player.currentWeapon = player.weapons.find(i => i.name === "Knife");
      player.currentMagicAttack = player.magicAttacks.find(i => i.name === "Green Orb");

      db.set(message.author.id, player);

      let doneEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.green)
        .setDescription(`Done setting up **${db.fetch(message.author.id).name}**! That was easy right?\n\nI gave you a knife and a magic attack to help you slay some enemies.\n\nYou can check your stats at any time with ${bot.config.prefix}stats or you can run ${bot.config.prefix}adventure to start slaying some monsters!\n\nFor any other commands run ${bot.config.prefix}help and take a look for yourself!`);

      message.channel.send(doneEmbed);

    }).catch(err => {
      console.log(err)
      message.channel.send(err.message);
    })
  }
};