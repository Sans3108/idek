const Discord = require("discord.js");

module.exports = {
  name: "reload",
  description: "Reloads a command.",
  usage: "<command>",
  //aliases: ["", ""],
  group: "dev",
  cooldown: 2,
  args: true,
  ownerOnly: true,
  guildOnly: false,
  execute: async (message, args, bot) => {
    const commandName = args[0].toLowerCase();
    const command = message.client.commands.get(commandName)
      || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) {
      let noCommandFound = new Discord.MessageEmbed()
        .setColor(bot.config.color.red)
        .setDescription(`There is no command with name or alias \`${commandName}\`, ${message.author}!`);

      return message.channel.send(noCommandFound);
    }

    delete require.cache[require.resolve(`./${command.name}.js`)];

    try {
      const newCommand = require(`./${command.name}.js`);
      message.client.commands.set(newCommand.name, newCommand);

      let successEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.green)
        .setDescription(`Command \`${command.name}\` was reloaded!`);

      message.channel.send(successEmbed);
    } catch (error) {
      console.error(error);
      let errorEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.red)
        .setDescription(`There was an error while reloading the \`${command.name}\`command, here's some info:\n\`${error.message}\``);

      message.channel.send(errorEmbed);
    }
  }
};