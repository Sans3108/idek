const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "stats",
  description: "Check your stats!",
  //usage: "",
  //aliases: ["", ""],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot) => {
    let playerFound = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription(`I don't think you have any stats to look at...\nRun \`${bot.config.prefix}start\` to get started!`);
    if (db.fetch(message.author.id) === null) return message.channel.send(playerFound);

    let player = db.fetch(message.author.id);

    let statsEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.blue)
      .setTitle(`__**${player.name}'s stats:**__`)
      .setThumbnail(message.author.displayAvatarURL({ dynamic: true }))
      .addField(`Level:`, `**${player.level}** (**${player.xp}/${player.nextLevel} xp**)`, true)
      .addField(`HP:`, `**${player.hp}/${player.maxHp} hp**`, true)
      .addField(`Added Damage per Attack:`, `**${player.addedDamage}**`, true)
      .addField(`Savings:`, `**${player.gold}** gold\n**${player.diamonds}** diamonds\n**${player.crystalShards}** crystal shards`)
      .addField(`Power:`, `**${player.stamina}/${player.maxStamina}** stamina\n**${player.mana}/${player.maxMana}** mana`)
      .addField(`Equipment:`, `Weapon: **${player.currentWeapon.name}**\nMagic Attack: **${player.currentMagicAttack.name}**`);


    message.channel.send(statsEmbed);
  }
};