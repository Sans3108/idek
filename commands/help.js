const Discord = require("discord.js");

module.exports = {
  name: "help",
  description: "Lists all my commands or info about a specific command.",
  usage: "[command name]",
  aliases: ["commands", "cmds"],
  group: "info",
  cooldown: 3,
  args: false,
  ownerOnly: false,
  guildOnly: false,
  execute: async (message, args, bot) => {
    const data = [];
    const { commands } = message.client;

    if (!args.length) {
      const helpEmbed = new Discord.MessageEmbed()
        .setTitle("**Here's a list of all my commands:**")
        .setThumbnail(bot.user.avatarURL())
      helpEmbed.setColor(bot.config.color.blue);
      helpEmbed.setFooter(`You can send "${bot.config.prefix}help [command name]" to get info on a specific command!`);

      const groups = ["info", "general"];
      groups.forEach(item => {
        let group = commands
          .filter(c => c.group === item)
          .map(command => "_`" + bot.config.prefix + command.name + "`_" + ` - ${command.description}`)
          .join("\n");

        helpEmbed.addField(`**${item.charAt(0).toUpperCase() + item.slice(1)} commands:**`, group);
      });

      return message.channel.send(helpEmbed);
    }

    const name = args[0].toLowerCase();
    const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

    if (!command) {
      let invalidCommandEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.red)
        .setDescription(`${message.author}, that command doesn't exist!`);

      return message.channel.send(invalidCommandEmbed);
    }

    data.push(`**Name:** ${command.name}`);

    if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
    if (command.description) data.push(`**Description:** ${command.description}`);
    if (command.usage) data.push(`**Usage:** ${bot.config.prefix}${command.name} ${command.usage}`);

    const commandCooldown = command.cooldown || 3;

    data.push(`**Cooldown:** ${bot.f.formatTime(commandCooldown)}`);

    const helpEmbed2 = new Discord.MessageEmbed()
      .setColor(bot.config.color.blue)
      .setDescription(data, { split: true });

    message.channel.send(helpEmbed2);
  }
};