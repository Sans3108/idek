const Discord = require("discord.js");
const db = require('quick.db');
const shopItems = require('../shop.js')

module.exports = {
  name: "tavern",
  description: "Get yourself some items for the fight!",
  usage: "<item name>",
  aliases: ["shop"],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot) => {
    let playerFound = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription(`I cannot let outsiders see my stuff before they're ready to see them!\nRun \`${bot.config.prefix}start\` to get started!`);
    if (db.fetch(message.author.id) === null) return message.channel.send(playerFound);

    let player = db.fetch(message.author.id);

    let busyEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.orange)
      .setDescription('You look a little busy, how about trying later?');
    if (player.busy) return message.channel.send(busyEmbed);

    if (!args.length) {
      let shopArrGold = shopItems.filter(obj => obj.price.type === 'gold').sort((a, b) => b.price.amount < a.price.amount).map(a => `\`${a.name}\` - ${a.price.amount} Gold`); //.join("\n");
      let shopArrDiamonds = shopItems.filter(obj => obj.price.type === 'diamonds').sort((a, b) => b.price.amount < a.price.amount).map(a => `\`${a.name}\` - ${a.price.amount} Diamonds`);
      let shopArrShards = shopItems.filter(obj => obj.price.type === 'shards').sort((a, b) => b.price.amount < a.price.amount).map(a => `\`${a.name}\` - ${a.price.amount} Crystal Shards`);

      let shopEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.blue)
        .setTitle(`__Welcome to ${message.guild.me.displayName}'s tavern!__`)
        .setThumbnail(bot.user.displayAvatarURL())
        .addField(`Gold items:`, `${shopArrGold}`)
        .addField(`Diamond items:`, `${shopArrDiamonds}`)
        .addField(`Crystal Shard items:`, `${shopArrShards}`)
        .setFooter(`Use '${bot.config.prefix}tavern <item name>' to see more info about an item!\nUse '${bot.config.prefix}buy' to buy an item!`);
      return message.channel.send(shopEmbed);
    }

    let shopArr = shopItems.map(a => a.name.toLowerCase());

    let invalidItemEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription(`I don't think I have \`${args.join(' ')}\` in my shop :/`);

    if (bot.f.arrayContains(args.join(' ').toLowerCase(), shopArr)) {
      let item = shopItems.find(i => i.name.toLowerCase() === args.join(' ').toLowerCase());

      let itemInfoEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.blue)
        .setTitle('Item info:')
        .setThumbnail(bot.user.displayAvatarURL())
        .addField('Name:', item.name)
        .addField('Description:', item.description);

      let currency;
      if (item.price.type === 'gold') {
        currency = 'Gold'
      } else if (item.price.type === 'diamonds') {
        currency = 'Diamonds'
      } else {
        currency = 'Crystal Shards'
      }

      itemInfoEmbed
        .addField('Price:', `${item.price.amount} ${currency}`)
        .setFooter(`Use '${bot.config.prefix}buy <item name>' to buy an item!`);
      return message.channel.send(itemInfoEmbed);

    } else return message.channel.send(invalidItemEmbed);

  }
};