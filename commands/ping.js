const Discord = require("discord.js");

module.exports = {
  name: "ping",
  description: "Pong!",
  //usage: "",
  //aliases: ["", ""],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot) => {
    const initialEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.blue)
      .setDescription("Pinging...");

    message.channel.send(initialEmbed).then(sent => {
      const finalEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.green)
        .setDescription(`Pong! :ping_pong:\n\n:person_running: Response Time: ${sent.createdTimestamp - message.createdTimestamp}ms`);

      sent.edit(finalEmbed);
    });
  }
};