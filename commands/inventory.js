const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "inventory",
  description: "Check your equipment and items!",
  //usage: "",
  aliases: ["weapons", "inv", "equipment"],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot) => {
    let playerFound = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription(`Your pockets look rather empty... Maybe start your adventure first?\nRun \`${bot.config.prefix}start\` to get started!`);
    if (db.fetch(message.author.id) === null) return message.channel.send(playerFound);

    let player = db.fetch(message.author.id);

    let normalWeapons = player.weapons.map(i => i.name).join(', ');
    let magicWeapons = player.magicAttacks.map(i => i.name).join(', ');
    let inventory = player.inventory;

    let equipmentEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.blue)
      .setTitle(`__**${player.name}'s inventory:**__`)
      .setThumbnail(message.author.displayAvatarURL({ dynamic: true }))
      .addField(`__Equipped weapon__`, `Name: **${player.currentWeapon.name}**\nDamage: **${player.currentWeapon.damage}**\nDurability: **${player.currentWeapon.durability}/${player.currentWeapon.maxDurability}**\nDurability taken per turn: **${player.currentWeapon.perUse}**\nStamina per use: **${player.currentWeapon.staminaCost}**`)
      .addField(`__Equipped magic weapon__`, `Name: **${player.currentMagicAttack.name}**\nDamage: **${player.currentMagicAttack.damage}**\nDurability: **${player.currentMagicAttack.durability}/${player.currentMagicAttack.maxDurability}**\nDurability taken per turn: **${player.currentMagicAttack.perUse}**\nMana per use: **${player.currentMagicAttack.manaCost}**`)
      .addField(`__Weapons inventory:__`, `**Normal Weapons:**\n${normalWeapons}\n**Magic Weapons:**\n${magicWeapons}`);

    if (inventory[0]) {
      inventory = inventory.map(i => i.name).join(', ');
      equipmentEmbed.addField(`__Items inventory__`, `${inventory}`);
    } else {
      equipmentEmbed.addField(`__Items inventory__`, `_Empty :/_`);
    }

    message.channel.send(equipmentEmbed);

  }
};