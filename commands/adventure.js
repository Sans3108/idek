const Discord = require("discord.js");
const db = require('quick.db');
const _ = require('lodash');

const monsters = require('../monsters.js');

module.exports = {
  name: "adventure",
  description: "Start a new adventure and take on some monsters in exchange for shiny rewards!",
  //usage: "",
  //aliases: ["", ""],
  group: "general", //info, dev
  cooldown: 1,
  args: false,
  ownerOnly: true,
  guildOnly: true,
  execute: async (message, args, bot) => {
    let playerFound = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setDescription(`How do you expect to survive without a weapon?\nRun \`${bot.config.prefix}start\` to get started!`);

    if (db.fetch(message.author.id) === null) return message.channel.send(playerFound);

    let p = db.fetch(message.author.id);

    let busyEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.color.orange)
      .setDescription('You look a little busy, how about trying later?');
    if (p.busy) return message.channel.send(busyEmbed);

    p.busy = true;
    db.set(message.author.id, p);

    let player = db.fetch(message.author.id);
    let boss = bot.f.arrayRandom(monsters);

    // looping functions, one calls the other
    // to start the thing call one of them

    let icon = bot.user.avatarURL();

    let emb = new Discord.MessageEmbed()
      .setColor(bot.f.config.color.orange)
      .setTitle(`**Adventure Time!**`)
      .setThumbnail(icon);

    let msg = await message.channel.send(emb);



    const filter = m => m.author.id === message.author.id
    const collector = msg.channel.createMessageCollector(filter, { time: 60000 });

    collector.on('collect', m => {
      collector.end();
    });

    collector.on('end', collected => {
      if (!collected.first()) {
        let noResponse = new Discord.MessageEmbed()
          .setColor(bot.config.color.red)
          .setDescription(`Guess you don't want to go on an adventure today... :/`)
        message.channel.send(noResponse);
      } else {
        message.channel.send(collected.map(m => m.content).join('\n'))
      }
    });

    let p2 = db.fetch(message.author.id);
    p2.busy = false;
    db.set(message.author.id, p2);
  }
};