module.exports = [
  {
    name: "Knife",
    type: "normal",
    staminaCost: 2,
    damage: 2,
    critRate: 5,
    durability: 20,
    maxDurability: 20,
    perUse: 1
  },
  {
    name: "Katana",
    type: "normal",
    staminaCost: 3,
    damage: 5,
    critRate: 15,
    durability: 40,
    maxDurability: 40,
    perUse: 1
  },
  {
    name: "Bow",
    type: "normal",
    staminaCost: 5,
    damage: 12,
    critRate: 22,
    durability: 75,
    maxDurability: 75,
    perUse: 3
  },
  {
    name: "Green Orb",
    type: "magic",
    manaCost: 5,
    damage: 5,
    critRate: 10,
    durability: 2,
    maxDurability: 2,
    perUse: 1
  },
  {
    name: "Arrow of Despair",
    type: "magic",
    manaCost: 15,
    damage: 20,
    critRate: 30,
    durability: 4,
    maxDurability: 4,
    perUse: 1
  },
  {
    name: "Dark Dynamite",
    type: "magic",
    manaCost: 50,
    damage: 100,
    critRate: 60,
    durability: 2,
    maxDurability: 2,
    perUse: 1
  }
]