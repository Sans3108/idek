require('dotenv').config();

const Discord = require('discord.js');
const fs = require('fs');
const db = require('quick.db');

const config = require('./config.json');
const f = require('./f.js');

let bot = new Discord.Client({ fetchAllMembers: true });

bot.config = config;
bot.f = f;
bot.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  bot.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

bot.on('ready', () => {
  console.log(`Ready!\nLogged in as ${bot.user.tag}`);
});

bot.on('message', async message => {
  if (message.content === "```xl\nPromise { <pending> }\n```" && message.author.id === bot.user.id) {
    message.delete();
  }

  if (!message.content.startsWith(bot.config.prefix) || message.author.bot) return;

  const args = message.content.slice(bot.config.prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();

  const command = bot.commands.get(commandName) || bot.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
  if (!command) return;

  if (!message.guild.me.hasPermission('ADMINISTRATOR')) return console.error(`Administrator permission missing, command ${command.name} not executed.\nGuild: ${message.guild.name}\nUser: ${message.author.tag}`);

  if (command.args && !args.length) {
    let noArgsEmb = new Discord.MessageEmbed()
      .setColor(bot.config.color.red);

    let reply = `You didn't provide any arguments, ${message.author}!`;

    if (command.usage) {
      reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
    }

    noArgsEmb.setDescription(reply);

    return message.channel.send(noArgsEmb);
  }

  if (command.guildOnly && message.channel.type !== 'text') return;

  if (command.ownerOnly && message.author.id !== bot.config.ownerID) return;

  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 3) * 1000;

  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;

      let cooldownEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.color.red)
        .setDescription(`(Cooldown) Please wait ${bot.f.formatTime(timeLeft)} before reusing the \`${command.name}\` command, ${message.author}!`);

      return message.channel.send(cooldownEmbed);
    }
  }

  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  try {
    command.execute(message, args, bot);
  } catch (error) {
    console.error(error);

    let errEmb = new Discord.MessageEmbed()
      .setColor(bot.config.color.red)
      .setTitle(`Well this wasn't supposed to happen...`)
      .addField(`Please contact Sans#0001 and show them this:`, `${error.message}`);

    message.channel.send(errEmb);
  }
});

bot.login(process.env.TOKEN);