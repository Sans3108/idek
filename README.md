# Nattu
A Discord bot made with discord.js v12 and quick.db running on Node v12.18.2 meant to be a source of fun in boring servers.

## What is the point of this?
Actually there's no point to it, it's just a test to see if I'm able to do make it without going insane.

## What exactly is this "bot"?
A Discord bot which I plan to turn into a text story kind of game in which you have a character (or multiple?) which you can use to progress into the story, there's no story as of now but I'll think of something...

I'm thinking of stuff to add such as abilities, magical attacks, character classes, mana, a currency system and a crap ton more.

## That sounds kinda neat, can I see this thing in action?
Sure! I made a [discord server](https://discord.gg/ZgkZDhZ) just for testing. The bot is not yet done so it will only be online when I work on it and test it. Maybe I'll make a proper support server... who knows.

If you do join it please know you will be a test subject right off the bat and I can't guarantee that you will like it...
There's no rules so unless u make angry you're gucci :)

Here's the [link](https://discord.gg/ZgkZDhZ).
