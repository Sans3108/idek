const weapons = require('./weapons.js');

module.exports = [
  {
    name: "Sp4rKy",
    hp: 200,
    mana: 100,
    maxMana: 100,
    magicAttack: weapons.find(w => w.name === 'Green Orb'),
    stamina: 20,
    maxStamina: 20,
    weapon: weapons.find(w => w.name === 'Knife')
  }
]