const items = require('./items.js');
const weapons = require('./weapons.js');

module.exports = [
  {
    name: "Goodie Bag",
    description: `Small bag of random goodies filled up with random prizes... it's shiny.`,
    price: {
      type: 'gold', //diamonds, shards
      amount: 100
    },
    goldDrops: {
      min: 2,
      max: 100,
      chance: 60
    },
    diamondDrops: {
      min: 0,
      max: 10,
      chance: 20
    },
    shardDrops: {
      min: 0,
      max: 1,
      chance: 5
    },
    weaponDrops: {
      objects: [weapons[0], weapons[1], weapons[2], weapons[3], weapons[4]],
      count: 1,
      chance: 10
    },
    itemDrops: {
      objects: [items[0]],
      count: 1,
      chance: 10
    }
  },
  {
    name: "Prop Item",
    description: `pp time`,
    price: {
      type: 'diamonds', //gold, diamonds, shards
      amount: 69
    },
    goldDrops: {
      min: 2,
      max: 100,
      chance: 60
    },
    diamondDrops: {
      min: 0,
      max: 10,
      chance: 20
    },
    shardDrops: {
      min: 0,
      max: 1,
      chance: 5
    },
    weaponDrops: {
      objects: [weapons[0], weapons[1], weapons[2], weapons[3], weapons[4]],
      count: 1,
      chance: 10
    },
    itemDrops: {
      objects: [items[0]],
      count: 1,
      chance: 10
    }
  },
  {
    name: "Prop Item 2",
    description: `big pp time?`,
    price: {
      type: 'shards', //gold, diamonds, shards
      amount: 420
    },
    goldDrops: {
      min: 2,
      max: 100,
      chance: 60
    },
    diamondDrops: {
      min: 0,
      max: 10,
      chance: 20
    },
    shardDrops: {
      min: 0,
      max: 1,
      chance: 5
    },
    weaponDrops: {
      objects: [weapons[0], weapons[1], weapons[2], weapons[3], weapons[4]],
      count: 1,
      chance: 10
    },
    itemDrops: {
      objects: [items[0]],
      count: 1,
      chance: 10
    }
  }
]