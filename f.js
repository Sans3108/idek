const db = require('quick.db');
const weapons = require('./weapons.js');
const Discord = require('discord.js');
const items = require('./shop.js')

module.exports = {
  getIDfromMention: function (a) {
    if (typeof a !== typeof "string")
      return new Error("Param 1 is not a string!");

    if (a.startsWith("<@") && a.endsWith(">")) {
      let id = a.slice(2, -1);
      if (id.startsWith("!")) {
        id = id.slice(1);
      }
      return id; //String (Discord ID)
    } else return new Error("Doesn't start with <@ and end with >");
  },
  randomNumber: function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min); //Integer
  },
  arrayContains: function (needle, arrhaystack) {
    return arrhaystack.includes(needle); //boolean
  },
  hasPing: function (id, message) {
    if (!id || !message) return new Error('Missing parameters!');

    let v;

    if (message.content.includes(`<@${id}>`) || message.content.includes(`<@!${id}>`)) {
      if (message.author.id !== id) {
        v = true;
      } else {
        v = false;
      }
    } else {
      v = false;
    }

    return v; //boolean
  },
  arrayRandom: function (arr) {
    if (!Array.isArray(arr)) return Error('Passed argument is not an array!');

    return arr[Math.floor((Math.random() * arr.length))];
  },
  chance: function (percentage) {
    let percent = parseInt(percentage);

    if (percent >= 0 && percent <= 100) { } else return new Error('Number must be between 0 - 100!');

    let arr = [];

    for (let i = 0; i < percent; i++) {
      arr.push(true);
    }

    let howMuch = parseInt(100 - arr.length);

    for (let j = 0; j < howMuch; j++) {
      arr.push(false);
    }

    return this.arrayRandom(arr); //boolean
  },
  arrayCount: function (what, array) {
    if (!Array.isArray(array)) return Error('Passed argument is not an array!');

    var count = 0;
    for (var i = 0; i < array.length; i++) {
      if (array[i] === what) {
        count++;
      }
    }
    return count; //Integer
  },
  formatTime: function (timeInSeconds) {
    const timeLeft = timeInSeconds
    let hours = Math.floor(timeLeft / 3600);
    let r1 = timeLeft % 3600;
    let minutes = Math.floor(r1 / 60);
    let seconds = Math.floor(r1 % 60);

    let finalTime;

    if (hours !== 0 && minutes !== 0 && seconds !== 0) {
      finalTime = `${hours} hour(s), ${minutes} minute(s) and ${seconds} second(s)`;
    } else if (hours !== 0 && minutes !== 0 && seconds === 0) {
      finalTime = `${hours} hour(s) and ${minutes} minute(s)`;
    } else if (hours !== 0 && minutes === 0 && seconds === 0) {
      finalTime = `${hours} hour(s)`;
    } else if (hours !== 0 && minutes === 0 && seconds !== 0) {
      finalTime = `${hours} hour(s) and ${seconds} second(s)`;
    } else if (hours === 0 && minutes !== 0 && seconds !== 0) {
      finalTime = `${minutes} minute(s) and ${seconds} second(s)`;
    } else if (hours === 0 && minutes !== 0 && seconds === 0) {
      finalTime = `${minutes} minute(s)`;
    } else if (hours === 0 && minutes === 0 && seconds === 0) {
      finalTime = `${seconds} second(s)`;
    } else if (hours === 0 && minutes === 0 && seconds !== 0) {
      finalTime = `${seconds} second(s)`;
    } else {
      finalTime = "`You should not see this message, contact staff ASAP!!!`";
    }

    return finalTime;
  },
  giveWeapon: function (wepName, playerID) {
    if (db.fetch(playerID) === null) return new Error('Player ID not found in database!');
    let wepArr = weapons.map(i => i.name);
    if (!wepArr.includes(wepName)) return new Error('Invalid weapon name! (Case sensitive)');
    let weapon = weapons.find(i => i.name === wepName);

    let player = db.fetch(playerID);

    if (weapon.type === 'normal') {
      player.weapons.push(weapon);
    } else {
      player.magicAttacks.push(weapon);
    }

    db.set(playerID, player);

    return true;
  },
  giveItem: function (itemName, playerID) {
    if (db.fetch(playerID) === null) return new Error('Player ID not found in database!');

    let itemArr = items.map(i => i.name);

    if (!itemArr.includes(itemName)) return new Error('Invalid weapon name! (Case sensitive)');

    let item = items.find(i => i.name === itemName);

    let player = db.fetch(playerID);

    player.inventory.push(item);

    db.set(playerID, player);

    return true;
  },
  takeItem: function (itemName, playerID) {
    if (db.fetch(playerID) === null) return new Error('Player ID not found in database!');

    let player = db.fetch(playerID);

    let itemArr = player.inventory.map(i => i.name);

    if (!itemArr.includes(itemName)) return new Error('Invalid item name! (Case sensitive)');

    let item = player.inventory.find(i => i.name === itemName);

    let index = itemArr.indexOf(item);

    player.inventory.splice(index, 1);

    db.set(playerID, player);

    return true;
  },
  checkLevel: function (playerID) {
    if (db.fetch(playerID) === null) return new Error('Player ID not found in database!');

    let player = db.fetch(playerID);

    var i = false;
    do {
      if (player.xp >= player.nextLevel) {
        player.level = player.level + 1;
        player.xp = player.xp - player.nextLevel;
        player.nextLevel = Math.floor(player.nextLevel + player.nextLevel * 0.2);
        player.maxHp = player.maxHp + 10;
        player.hp = player.hp + 10;
        player.addedDamage = player.addedDamage + 5;
        player.maxStamina = player.maxStamina + 2;
        player.stamina = player.stamina + 2;
        player.maxMana = player.maxMana + 10;
        player.mana = player.mana + 10;
      } else {
        i = true;
      }
    }
    while (i === false);

    db.set(playerID, player);

    return true;
  },
  addXP: function (playerID, amount) {
    if (db.fetch(playerID) === null) return new Error('Player ID not found in database!');

    let player = db.fetch(playerID);

    player.xp = player.xp + parseInt(amount);
    db.set(playerID, player);
    this.checkLevel(playerID);

    return true;
  }
};