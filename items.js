const db = require('quick.db');

module.exports = [
  {
    name: "Repair Stone",
    description: "Fully repairs the weapon of your choice! It will be good as new :D",
    function: function() {
      //...
    }
  },
  {
    name: "Healing Potion",
    description: "Heals all of your HP! No side-effects included ;)",
    function: function(playerID) {
      let player = db.fetch(playerID);
      player.hp = player.maxHp;
      db.set(playerID, player);
    }
  },
  {
    name: "Rename Token",
    description: "Use this to change your name!",
    function: function(playerID) {
      let player = db.fetch(playerID);
      player.hp = player.maxHp;
      db.set(playerID, player);
    }
  }
]